/*
 * Copyright 2011-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package xyz.ewangshi.jdbc;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.jdbc.core.RowMapper;

/**
 * @author ewangshi
 * 2016-07-06
 */
public class Product implements Serializable, RowMapper<Product> {

	private static final long serialVersionUID = 4713042221599784461L;

	private Integer id;
	private String name;
	private BigDecimal length;
	private BigDecimal width;
	private BigDecimal height;
	private BigDecimal price;
	private Date createTime;
	private Integer isDel;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getLength() {
		return length;
	}

	public void setLength(BigDecimal length) {
		this.length = length;
	}

	public BigDecimal getWidth() {
		return width;
	}

	public void setWidth(BigDecimal width) {
		this.width = width;
	}

	public BigDecimal getHeight() {
		return height;
	}

	public void setHeight(BigDecimal height) {
		this.height = height;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getIsDel() {
		return isDel;
	}

	public void setIsDel(Integer isDel) {
		this.isDel = isDel;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", length=" + length + ", width=" + width + ", height=" + height
				+ ", price=" + price + ", createTime=" + createTime + ", isDel=" + isDel + "]";
	}

	public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
		Product product = new Product();
		product.setId(rs.getInt("id"));
		product.setCreateTime(rs.getDate("create_time"));
		product.setHeight(rs.getBigDecimal("height"));
		product.setIsDel(rs.getInt("is_del"));
		product.setLength(rs.getBigDecimal("length"));
		product.setName(rs.getString("name"));
		product.setPrice(rs.getBigDecimal("price"));
		product.setWidth(rs.getBigDecimal("width"));
		return product;
	}
	
}
