/*
 * Copyright 2011-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package xyz.ewangshi.jdbc;

import java.util.HashSet;
import java.util.Set;

/**
 * Constant
 * @author ewangshi
 * 2016-07-06
 */
public final class SQLBeanConstant {

	public static final String REGEX = "(<\\[([^]>])*]>)|(<%([^%>])*%>)";
	public static final Set<String> RESERVED_WORD;

	static {
		RESERVED_WORD = new HashSet<String>();
		RESERVED_WORD.add("<\\[");
		RESERVED_WORD.add("]>");
		RESERVED_WORD.add("<%");
		RESERVED_WORD.add("%>");
	}
}
