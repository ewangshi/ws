/*
 * Copyright 2011-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package xyz.ewangshi.jdbc;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @author ewangshi
 * 2016-07-06
 */
public class SQLBean {

	private List<SqlParam> params;
	private String originalSql;
	private String sqlPart;

	public List<SqlParam> getParams() {
		return params;
	}

	public void setParams(List<SqlParam> params) {
		this.params = params;
	}

	public String getOriginalSql() {
		return originalSql;
	}

	public void setOriginalSql(String originalSql) {
		this.originalSql = originalSql;
	}

	public SQLBean(String originalSql) {
		// remove excess blank characters
		String sqlText = originalSql.trim().replaceAll("\\s{2,}", " ");
		this.originalSql = sqlText;
		init();
	}

	public String getSqlPart() {
		return sqlPart;
	}

	public void setSqlPart(String sqlPart) {
		this.sqlPart = sqlPart;
	}

	@Override
	public String toString() {
		return "SQLBean [params=" + params + ", originalSql=" + originalSql + ", sqlPart=" + sqlPart + "]";
	}

	private void init() {
		String sql = originalSql;
		params = new ArrayList<SqlParam>();

		Pattern pattern = Pattern.compile(SQLBeanConstant.REGEX);
		Matcher matcher = pattern.matcher(sql);
		int index = 0;
		StringBuffer sb = new StringBuffer();

		while (matcher.find()) {
			String str = matcher.group();
			SqlParam sqlParam = sqlParam(str);
			matcher.appendReplacement(sb, "{" + index++ + "}");
			params.add(sqlParam);
		}
		
		setSqlPart(matcher.appendTail(sb).toString());
	}

	private SqlParam sqlParam(final String sqlStr) {
		SqlParam sqlParam = null;
		String str = sqlStr;
		boolean isStr = sqlStr.startsWith("<%");

		for (String rw : SQLBeanConstant.RESERVED_WORD) {
			str = str.replaceAll(rw, "");
		}

		if (str.indexOf(":") != -1) {
			String[] arr = str.split(":");
			String key = arr[1];
			sqlParam = new SqlParam(key, str, isStr);
		}
		return sqlParam;
	}
}

class SqlParam {
	private String key;
	private String sqlStr;
	private boolean isStr;

	public SqlParam(String key, String sqlStr, boolean isStr) {
		this.key = key;
		this.sqlStr = sqlStr;
		this.isStr = isStr;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getSqlStr() {
		return sqlStr;
	}

	public void setSqlStr(String sqlStr) {
		this.sqlStr = sqlStr;
	}

	public boolean isStr() {
		return isStr;
	}

	public void setStr(boolean isStr) {
		this.isStr = isStr;
	}

	@Override
	public String toString() {
		return "SqlParam [key=" + key + ", sqlStr=" + sqlStr + ", isStr=" + isStr + "]";
	}

}